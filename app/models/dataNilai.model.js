module.exports = (sequelize, Sequelize) => {
    const DataNilai = sequelize.define("data_nilai", {
      nim: {
        type: Sequelize.INTEGER,
      },
      matkul_id: {
        type: Sequelize.INTEGER
      },
      dosen_id: {
        type: Sequelize.INTEGER
      },
      nilai: {
          type: Sequelize.INTEGER
      },
      keterangan: {
          type: Sequelize.STRING
      }
    });
  
    return DataNilai;
  };