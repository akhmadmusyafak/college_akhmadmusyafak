module.exports = (sequelize, Sequelize) => {
    const MataKuliah = sequelize.define("mata_kuliah", {
      matkul_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      nama_mata_kuliah: {
        type: Sequelize.STRING
      },
      nim: {
        type: Sequelize.INTEGER
      }
    });
  
    return MataKuliah;
  };