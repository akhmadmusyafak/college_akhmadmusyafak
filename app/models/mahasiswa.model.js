module.exports = (sequelize, Sequelize) => {
    const Mahasiswa = sequelize.define("mahasiswa", {
      nim: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      nama_mahasiswa: {
        type: Sequelize.STRING
      },
      alamat: {
        type: Sequelize.STRING
      },
      tanggal_lahir: {
          type: Sequelize.DATE
      },
      jurusan: {
          type: Sequelize.STRING
      }
    });
  
    return Mahasiswa;
  };