module.exports = (sequelize, Sequelize) => {
    const Dosen = sequelize.define("dosen", {
      dosen_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      nama_dosen: {
        type: Sequelize.STRING
      }
    });
  
    return Dosen;
  };