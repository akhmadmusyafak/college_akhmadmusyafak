const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../../models");
const User = db.user;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }
    req.userId = decoded.id;
    next();
  });
};

isDosen = (req, res, next) => {
    User.findByPk(req.userId).then(user => {
          if (user.role == 2) {
            next();
            return;
          }
        res.status(403).send({
          message: "Hanya dapat diakses Dosen!"
        });
        return;
      });
  };

  const authJwt = {
    verifyToken: verifyToken,
    isDosen: isDosen
  };
  
  module.exports = authJwt;
  