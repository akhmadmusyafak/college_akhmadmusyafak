module.exports = app => {
    const dataNilai = require("../controllers/dataNilai.controller");
  
    var router = require("express").Router();
  
    // Create a new Data Nilai
    router.post("/", dataNilai.create);
  
    // Update a Tutorial with nim, matkulId, dosenId
    router.put("/:nim/:matkulId/:dosenId", dataNilai.update);
  
    // Delete a Tutorial with nim, matkulId, dosenId
    router.delete("/:nim/:matkulId/:dosenId", dataNilai.delete);
  
    app.use('/api/data-nilai', router);
  };