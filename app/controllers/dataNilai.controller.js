const db = require("../models");
const DataNilai = db.dataNilai;
const Op = db.Sequelize.Op;

// Create and Save a new DataNilai
exports.create = (req, res) => {
    // Validate
    if (!req.body.nim || !req.body.matkulId || !req.body.dosenId || !req.body.nilai) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
        return;
      }
    
      // Create a dataNilai
      const dataNilai = {
        nim: req.body.nim,
        matkul_id: req.body.matkulId,
        dosen_id: req.body.dosenId,
        nilai: req.body.nilai,
        keterangan: req.body.keterangan
      };
    
      // Save data nilai in the database
      DataNilai.create(dataNilai)
        .then(data => {
          res.send(data);
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the Tutorial."
          });
        });
};

// Update a DataNilai by the nim,matkul id and dosen id in the request
exports.update = (req, res) => {
    const nim = req.params.nim
    const matkulId = req.params.matkulId
    const dosenId = req.params.dosenId

    DataNilai.update(req.body, {
      where: { nim:nim, matkul_id:matkulId, dosen_id:dosenId }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Data Nilai was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Data. Maybe Data was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Data nilai"
        });
      });
};

// Delete a Tutorial with the specified nim,matkul id and dosen id in the request
exports.delete = (req, res) => {
    const nim = req.params.nim
    const matkulId = req.params.matkulId
    const dosenId = req.params.dosenId

    Tutorial.destroy({
      where: { nim:nim, matkul_id:matkulId, dosen_id:dosenId }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Data nilai was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Data nilai. Maybe Data Nilai was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Data nilai"
        });
      });
};