var readlineSync = require('readline-sync');

value = readlineSync.question('Input value to convert: ');
convertInt(value);

function convertInt(value){
	var value, h, m, s;
	h = Math.floor(value/3600);
	m = Math.floor((value%3600)/60);
	s = value - (h*3600) - (m*60);

	if(h == 0 && m == 0)
		console.log( s + " detik." );
	else if(h == 0 && s == 0)
		console.log( m + " menit " );
	else if(m == 0 && s == 0)
		console.log( h + " jam " );
	else if(h == 0)
		console.log( m + " menit " + s + " detik." );
	else if(m == 0)
		console.log( h + " jam " + s + " detik." );
	else if(s == 0)
		console.log( h + " jam " + m + " menit." );
	else
		console.log( h + " jam " + m + " menit " + s + " detik." );
}
