-- ----------------------------
-- SOAL B
-- ----------------------------

-- Soal 2. Buatlah syntax SQL untuk insert beberapa data ke table dan kolom database tersebut

-- ----------------------------
-- INSERT TABLE users
-- ----------------------------
INSERT INTO users(email, password, role) VALUES
('abc@gmail.com', 'abc', 1),
('abcde@gmail.com', 'abcde', 2),
('abcdef@gmail.com', 'abcdef', 1),
('aaa123@gmail.com', 'aaa123', 2),
('example@gmail.com', 'pass012', 1);

-- ----------------------------
-- INSERT TABLE mahasiswa
-- ----------------------------
INSERT INTO mahasiswa(nama_mahasiswa, alamat, tanggal_lahir, jurusan) VALUES
('Musyafak', 'Kudus', '01-01-1991', 'Fisika'),
('Imam', 'Sumedang', '01-05-2000', 'Teknik Informatika'),
('Zioda', 'Jakarta', '01-06-1999', 'Sistem Informasi'),
('Rio', 'Bandung', '01-02-1996', 'Teknik Informatika'),
('Naufal', 'Cimahi', '01-03-2000', 'Teknik Sipil');

-- ----------------------------
-- INSERT TABLE dosen
-- ----------------------------
INSERT INTO dosen(nama_dosen) VALUES
('Muthia'), ('Taofik'), ('Hanif'), ('Ridwan'), ('Alif');

-- ----------------------------
-- INSERT TABLE mata kuliah
-- ----------------------------
INSERT INTO mata_kuliah(nama_mata_kuliah, nim) VALUES
('Statistika', 1), ('Matematika', 2), ('Fisika', 3), ('Biologi', 1), ('Matematika', 1), ('Biologi', 2), ('Matematika', 3);

-- ----------------------------
-- INSERT TABLE data nilai
-- ----------------------------
INSERT INTO data_nilai(nim, matkul_id, dosen_id, nilai, keterangan) VALUES
(1, 1, 1, 90, 'Terbaik'),
(2, 2, 2, 80, 'Cukup'),
(3, 3, 3, 60, 'Tingkatkan lagi');