-- ----------------------------
-- SOAL B
-- ----------------------------
/*
Untuk membuat database dengan nama college.sql :
CREATE DATABASE college;
*/

-- Soal 1. Buatlah syntax SQL untuk membuat database, table, dan kolom tersebut beserta tipe data,
-- keterangan value sebagai primary key, foreign key, dsb

-- ----------------------------
-- Sequence structure for user_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_user_id_seq";
CREATE SEQUENCE "public"."users_user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for mahasiswa_nim_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mahasiswa_nim_seq";
CREATE SEQUENCE "public"."mahasiswa_nim_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for dosen_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."dosen_dosen_id_seq";
CREATE SEQUENCE "public"."dosen_dosen_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for matkul_id_matkul_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."mata_kuliah_matkul_id_seq";
CREATE SEQUENCE "public"."mata_kuliah_matkul_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 987654321
START 1
CACHE 1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "user_id" int8 NOT NULL DEFAULT nextval('users_user_id_seq'::regclass),
  "email" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "role" int2 NOT NULL
)
;

-- ----------------------------
-- Table structure for dosen
-- ----------------------------
DROP TABLE IF EXISTS "public"."dosen";
CREATE TABLE "public"."dosen" (
  "dosen_id" int8 NOT NULL DEFAULT nextval('dosen_dosen_id_seq'::regclass),
  "nama_dosen" varchar(128) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS "public"."mahasiswa";
CREATE TABLE "public"."mahasiswa" (
  "nim" int8 NOT NULL DEFAULT nextval('mahasiswa_nim_seq'::regclass),
  "nama_mahasiswa" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "alamat" varchar(255) COLLATE "pg_catalog"."default",
  "tanggal_lahir" date,
  "jurusan" varchar(128) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for mata kuliah
-- ----------------------------
DROP TABLE IF EXISTS "public"."mata_kuliah";
CREATE TABLE "public"."mata_kuliah" (
  "matkul_id" int8 NOT NULL DEFAULT nextval('mata_kuliah_matkul_id_seq'::regclass),
  "nama_mata_kuliah" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "nim" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for data nilai
-- ----------------------------
DROP TABLE IF EXISTS "public"."data_nilai";
CREATE TABLE "public"."data_nilai" (
  "nim" int8 NOT NULL,
  "matkul_id" int8 NOT NULL,
  "dosen_id" int8 NOT NULL,
  "nilai" int4 NOT NULL,
  "keterangan" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_user_id_seq"
OWNED BY "public"."users"."user_id";
ALTER SEQUENCE "public"."dosen_dosen_id_seq"
OWNED BY "public"."dosen"."dosen_id";
ALTER SEQUENCE "public"."mahasiswa_nim_seq"
OWNED BY "public"."mahasiswa"."nim";
ALTER SEQUENCE "public"."mata_kuliah_matkul_id_seq"
OWNED BY "public"."mata_kuliah"."matkul_id";

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "pk_users" PRIMARY KEY ("user_id");
-- ----------------------------
-- Primary Key structure for table dosen
-- ----------------------------
ALTER TABLE "public"."dosen" ADD CONSTRAINT "pk_dosen" PRIMARY KEY ("dosen_id");
-- ----------------------------
-- Primary Key structure for table mahasiswa
-- ----------------------------
ALTER TABLE "public"."mahasiswa" ADD CONSTRAINT "pk_mahasiswa" PRIMARY KEY ("nim");
-- ----------------------------
-- Primary Key structure for table mata_kuliah
-- ----------------------------
ALTER TABLE "public"."mata_kuliah" ADD CONSTRAINT "pk_mata_kuliah" PRIMARY KEY ("matkul_id");

-- ----------------------------
-- Foreign Keys structure for table mata_kuliah
-- ----------------------------
ALTER TABLE "public"."mata_kuliah" ADD CONSTRAINT "fk_mata_kuliah_reference_mahasiswa" FOREIGN KEY ("nim") REFERENCES "public"."mahasiswa" ("nim") ON DELETE RESTRICT ON UPDATE RESTRICT;
-- ----------------------------
-- Foreign Keys structure for table data nilai
-- ----------------------------
ALTER TABLE "public"."data_nilai" ADD CONSTRAINT "fk_data_nilai_reference_mahasiswa" FOREIGN KEY ("nim") REFERENCES "public"."mahasiswa" ("nim") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."data_nilai" ADD CONSTRAINT "fk_data_nilai_reference_mata_kuliah" FOREIGN KEY ("matkul_id") REFERENCES "public"."mata_kuliah" ("matkul_id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."data_nilai" ADD CONSTRAINT "fk_data_nilai_reference_dosen" FOREIGN KEY ("dosen_id") REFERENCES "public"."dosen" ("dosen_id") ON DELETE RESTRICT ON UPDATE RESTRICT;
