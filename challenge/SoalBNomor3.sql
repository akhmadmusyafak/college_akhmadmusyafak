-- ----------------------------
-- SOAL B
-- ----------------------------

-- Soal 3. Buatlah syntax SQL untuk menampilkan semua data mahasiswa termasuk dengan umur
--(diambil dari kolom tanggal lahir), dosen, mata kuliah, nilai dengan filter nilai yang dibawah
--70 dengan urutan nilai paling besar di awal.

SELECT m.nim AS "NIM", m.nama_mahasiswa AS "Nama Mahasiswa",
	m.alamat AS "Alamat", m.tanggal_lahir AS "Tanggal Lahir", age(now()::date, m.tanggal_lahir) AS "Umur",
	m.jurusan AS "Jurusan", d.nama_dosen AS "Nama Dosen", mk.nama_mata_kuliah AS "Nama Mata Kuliah",
	dn.nilai AS "Nilai"
FROM mahasiswa AS m
LEFT JOIN data_nilai AS dn ON m.nim = dn.nim
LEFT JOIN mata_kuliah AS mk ON dn.matkul_id = mk.matkul_id
LEFT JOIN dosen AS d ON dn.dosen_id = d.dosen_id
WHERE dn.nilai < 70
ORDER BY dn.nilai DESC